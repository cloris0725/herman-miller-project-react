import React, {Component} from 'react';
import {BrowserRouter, Router, Route, Link, useHistory, useLocation, Switch} from "react-router-dom";
import './App.css';
import Header from "./components/mainPage/header/header";
import Main from "./components/mainPage/mainContent/mainPageSortLine";
import Footer from "./components/mainPage/footer/footer";
import ProductPage from "./components/productPage/productPage";



function App() {


    return(
            <div className="project">
                <BrowserRouter>
                    <Header />
                    <Switch>
                        <Route path="/" exact component={Main}/>
                        <Route path="/ProductPage/:id" exact component={ProductPage}/>
                    </Switch>
                    <Footer />
                </BrowserRouter>
            </div>
    )


}



export default App;
