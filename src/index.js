import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import {Provider} from "react-redux";
import {applyMiddleware, createStore} from "redux";
import thunk from "redux-thunk";
import reducer from "./reducer/reducer";





ReactDOM.render(
  <Provider store={createStore(reducer, applyMiddleware(thunk))}>
      <App />
  </Provider>,
  document.getElementById('root')
);


