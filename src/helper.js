export const GET_ALL_IMG = "GET_ALL_IMG"

export const SHOW_ALERT = "SHOW_ALERT"

export const SHOW_COLOR = "SHOW_COLOR"

export const GET_ONE_CHAIR_ARR = "GET_ONE_CHAIR_ARR"

export const ONE_Item = "ONE_Item"

export const ONE_CATEGORY = "ONE_CATEGORY"

export const GET_SUBTOTAL = "GET_SUBTOTAL"

export const GET_TOTAL = "GET_TOTAL"