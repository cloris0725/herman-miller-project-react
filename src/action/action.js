import axios from "axios";
import {GET_ALL_IMG,SHOW_ALERT,GET_ONE_CHAIR_ARR} from "../helper";

const getAllPictures = () => async dispatch => {
    let res = undefined
    let allDataArray = undefined

    try{
        res = await axios.get("http://api-ecommerce.mark2win.com/product");
        //console.log('--->http return:', res)
        //console.log('--->http return:', res.data)
        // console.log('--->http return:', res.data.data)
        allDataArray = res.data.data
        console.log('imgArray--->', allDataArray)
        for(let i = 0; i < allDataArray.length; i++){
            // console.log(res.data.data[i].media)
            // imgArray.push(res.data.data[i].media)
            allDataArray[i].media = allDataArray[i].media.split("|")

        }



    }catch (err){
        console.log(err)
        return dispatch({
            type: SHOW_ALERT,
            payload: err.message

        })
    }
    // console.log('--->http return:', res)
    dispatch({
            type: GET_ALL_IMG,
            payload: allDataArray,


        }
    )
    console.log("see--->", allDataArray)
}



export const getProductDetails =(id) => async dispatch => {

    let oneChairArray = undefined

    try{
        let res = await axios.get(`http://api-ecommerce.mark2win.com/product/${id}`);
        // console.log('newAction--->', res)
        oneChairArray = res.data.data
        // console.log("oneChairData", oneChairArray)
        oneChairArray.media = oneChairArray.media.split("|")

    }catch(err){
        console.log(err)
        return dispatch({
            type: SHOW_ALERT,
            payload: err.message

        })

    }

    dispatch({
            type: GET_ONE_CHAIR_ARR,
        payload: oneChairArray



        }
    )

}


export default getAllPictures
