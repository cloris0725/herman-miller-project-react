import {GET_SUBTOTAL} from "../helper";


const getSubTotal = (subtotal) => dispatch => {

    dispatch({
        type: GET_SUBTOTAL,
        payload: subtotal
    })

}


export default getSubTotal