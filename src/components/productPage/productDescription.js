import React, {Component} from "react";
import "../css/productPage/productDescription.css"

class Description extends Component{

    render() {
        return (
            <div className="description-container">
                <div className="text-container">
                    <div className="title-container">
                        Description
                    </div>
                    <div className="main-container">
                        <div className="main-left">
                            <p>
                                The Aeron Chair combines a deep knowledge of human-centered design with cutting-edge technology. With over 7 million sold, our most admired and recognized work chair still sets the benchmark for ergonomic comfort more than 20 years after its debut.
                            </p>
                        </div>

                        <div className="main-right">
                            <div className="right-top">Key Features</div>
                            <div className="right-bottom">
                                <ul className="list">
                                    <li>
                                        12-year warranty
                                    </li>
                                    <li>
                                        Inclusive design with 3 distinct sizes
                                    </li>
                                    <li>
                                        Breathable seat and back
                                    </li>
                                    <li>
                                        Pellicle 8Z® provides 8 zones of varying tension for sophisticated support
                                    </li>
                                    <li>
                                        Made in Michigan at a 100% green-energy facility
                                    </li>
                                    <li>
                                        Adjustable tilt and seat angle
                                    </li>
                                    <li>
                                        Superior back support
                                    </li>
                                    <li>
                                        Fully adjustable arms (height, depth and angle)
                                    </li>
                                    <li>
                                        For questions about lead times, in-stock options or delivery please give us a call at 888.798.0202
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="feature-content">
                    <div className="content-container">
                        <div className="title">
                            <h4>Featured Images</h4>
                        </div>
                        <div className="img-container">
                            <div className="all-img">
                                <div className="img-top">
                                    <img src="https://store.hermanmiller.com/on/demandware.static/-/Sites-master-catalog/default/dwe669e7b2/gallery/2195348/sm_2195348-gallery1.jpg" alt=""/>
                                    <img src="https://store.hermanmiller.com/on/demandware.static/-/Sites-master-catalog/default/dw9b6c3f97/gallery/2195348/sm_2195348-gallery2.jpg" alt=""/>
                                    <img src="https://store.hermanmiller.com/on/demandware.static/-/Sites-master-catalog/default/dwaecb9040/gallery/2195348/sm_2195348-gallery9.jpg" alt=""/>
                                </div>
                                <div className="img-bottom">
                                    <img src="https://store.hermanmiller.com/on/demandware.static/-/Sites-master-catalog/default/dwcafd1d7a/gallery/2195348/sm_2195348-gallery4.jpg" alt=""/>
                                    <img src="https://store.hermanmiller.com/on/demandware.static/-/Sites-master-catalog/default/dw71c616b8/gallery/2195348/sm_2195348-gallery5.jpg" alt=""/>
                                    <img src="https://store.hermanmiller.com/on/demandware.static/-/Sites-master-catalog/default/dw63ea3d61/gallery/2195348/sm_2195348-gallery6.jpg" alt=""/>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        );
    }
}


export default Description
