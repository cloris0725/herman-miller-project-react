import React, {Component} from "react";
import "../css/productPage/afmsFrameCaster.css"
import {connect} from "react-redux"
import getSubTotal from "../../action/subTotalAction";



//notes:
// const Checkbox = props => {
//     return (<div>
//        {props.checked ? <i className="far fa-circle" /> : <i className="far fa-check-circle" />}
//     </div>)
// }
//
// const PItem  = props => {
//     return ( props.pis.map(pc =>
//             pc.map(pi => (<Checkbox key={pi.id} item={pi}/>))
//         )
//     )
// }

class Selection extends Component {

    itemPriceArr = []




   state = {
        show: true
   }


    sum = () => {
        let total = 0
        let totalArr = this.itemPriceArr
        for (let i = 0; i < totalArr.length; i ++){
            total += totalArr[i]


        }
        // console.log("total==>", total)
        return total.toFixed(2)
    }




    clickedItem = (id, clickedIndex) => {
        // console.log("id===>", id)
        // console.log("clickedIndex===>", clickedIndex)
        let {show} = this.state
        let allItem = this.props.oneChairCategory?.profileCategories?.[id].profileItems
        // console.log(allItem)
        for (let i=0; i< allItem.length; i++){
            allItem[i].checked = false
            this.itemPriceArr = []
            // allItem[i].price = 0
            if(i === clickedIndex) {
                allItem[i].checked = true
            }
        }
        this.setState({show:!show})
    }


    render(){
        let category = this.props.oneChairCategory?.profileCategories
        // console.log("checkcheck===>", category)
        return (
            <div className="selection">
                {category?.map((oneCategory, id) => {

                    if (oneCategory.name !== "Frame / Base"){

                        return (
                            <div className="profile" key={id}>
                               <div className="item-title">
                                   <span>{oneCategory?.name}</span>
                               </div>
                                <div className="item-body">
                                    {oneCategory.profileItems?.map((eachItem, index) => {
                                        return(
                                            <div className="body-content" key={index} >

                                                { eachItem.checked?
                                                    <div className="checked"  >
                                                        <div className="icons" >
                                                            <i id="check" className="far fa-check-circle" />

                                                        </div>
                                                        <div className="eachItemBold">
                                                            <span className="item-name">{eachItem.name}</span>
                                                            <span style={{display:"none"}}>{this.itemPriceArr.push(parseInt(eachItem.price))}</span>
                                                            {/*<span style={{display:"none"}}>{subTotal.push(eachItem.price)}</span>*/}
                                                            {/*{itemPriceArr.push(eachItem.price)}*/}
                                                        </div>

                                                    </div>:
                                                    <div className="unChecked" >
                                                        <div className="icons" >


                                                            <i id="circle" className="far fa-circle"  onClick={event => {this.clickedItem(id, index)}}/>
                                                        </div>
                                                        <div className="eachItem">
                                                            <span className="item-name">{eachItem.name}</span>
                                                            <span className="item-price">{`+ $ ${eachItem.price}`}</span>
                                                            {/*<span style={{display:"none"}}>{this.clickedItemPriceArr.push(parseFloat(eachItem.price))}</span>*/}
                                                        </div>
                                                    </div>}
                                                {/*{this.itemArr.push(index, eachItem)}*/}



                                            </div>
                                        )
                                    })}



                                </div>

                            </div>
                        )
                    }else {
                        return (
                            <div key={id}>
                                <div className="frame">
                                    <div className="frame-title">
                                        <div>{oneCategory?.name}</div>
                                    </div>
                                    <div className="frame-body">
                                        <div className="top">
                                            <span>Graphite / Graphite</span>

                                        </div>
                                        <div className="middle">
                                            {oneCategory.profileItems?.map((eachFrame, index) => {
                                                // console.log("eachFrameObj===>", eachFrame)
                                                let img = eachFrame.media
                                                return(
                                                    <div key={index}>
                                                        {eachFrame.checked?
                                                            <div className="checked-img" key={index}>
                                                                <img src={img} alt=""/>
                                                                <span style={{display:"none"}}>{this.itemPriceArr.push(parseInt(eachFrame.price))}</span>
                                                            </div>:
                                                            <div className="unChecked" onClick={event => {this.clickedItem(id, index)}}>
                                                                <img src={img} alt=""/>
                                                            </div>
                                                        }
                                                    </div>
                                                )
                                            })}
                                        </div>
                                        <div className="bottom">
                                            <button>Request Free Swatches</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        )
                    }

                        //console.log("checkCategory==>", category)
                    })
                }
                <div>
                    {/*{`Subtotal: $ ${this.sum()}`}*/}
                </div>
            </div>


        )

    }


    //
    componentDidUpdate(prevProps, prevState, snapshot) {
        this.props.getSubTotal(this.sum())
    }


}





const mapStateToProps = (state, ownProps) => {

    return {
        oneChairCategory: state.oneChairReducer.oneChairData
    }

}

export default connect(mapStateToProps, {getSubTotal})(Selection)



