import React, {Component, useState} from "react";
import {connect} from "react-redux"
import "../css/productPage/productPage.css"
import Selection from "./armsFrameCaster";
import {getProductDetails} from "../../action/action";
import Description from "./productDescription";
import "../css/productPage/checkoutBar.css"
import Truck from "../../img/Truck.svg"
// import getTotal from "../../action/finalPriceAction";




class ProductPage extends Component{


    state = {
        reRender: [],
        price: true,
        quantity: 1,
        total: 0
        //data: []
    }

    testFunction = (e, subtotal) => {
        console.log("on mouse over===>", e.target.value)
        // console.log("omg===>", subtotal)
        let finalPrice = 0
        let quantity = e.target.value
        if (!isNaN(quantity)){

            finalPrice = subtotal * quantity
            console.log("final!!!!", finalPrice)


        }else {
            finalPrice = subtotal
            console.log("second choice===>", finalPrice)
        }
        return finalPrice


    }

    sumFunction = () => {
        // console.log("try===>", e.target.value)
        // console.log("checkcheck===>", parseFloat(this.props.oneChairCategory.price) )
        // console.log("original price===>", parseFloat(this.props.oneChairCategory?.price))
        let a =  parseFloat(this.props.oneChairCategory.price)
        let b =  parseFloat(this.props.subtotal)
        // let quantity = parseInt(e.target.value)
        if (!isNaN(a) && !isNaN(b)){
            let total = (a+ b).toFixed(2)

            return total
        }

    }

    render() {
        // console.log("hi")
        // const data2 = this.props?.oneChairData
        // console.log("datadata===>", data2)

        // let data = this.props.location.state
        let index = 0
        // console.log("super chou!!!!", this.test)
        // console.log("also super chou!!!", this.subtotal)

        return (

            <div className="productPage">
                <div className="productPage-content">
                    <div className="content-intro">
                        <span>office > office Chairs > {this.props.oneChairCategory.name}</span>
                    </div>
                    <div className="content-main">
                        <div className="sticky">
                            <div className="product-img-column">
                                {this.props.oneChairCategory.media?.map((eachImgArray, index) => {

                                    let img = eachImgArray
                                    // console.log('it is img--->',img)
                                    // console.log('new-->',this.props.history.location.state.chairImgArray)
                                    return(
                                        <div className="images-showing" key={index}>
                                            <img src={img} alt=""/>
                                        </div>
                                    )
                                })}

                            </div>
                            <div className="product-img-centre">
                                <img className="centre" src={this.props.oneChairCategory?.media?.[index]} alt=""/>
                            </div>
                        </div>


                        <div className="product-details">
                            <div className="details-top">
                                <div className="top-1">{this.props.oneChairCategory.name}</div>
                                <div className="top-2">Designed by Don Chadwick and Bill Stumpf</div>
                                <div className="top-3">
                                    {this.sumFunction()}
                                </div>
                                <div className="top-4">
                                    <div className="top4-div">
                                        <i className="fas fa-check"></i>
                                        <a className="div-a" href="#">12-Year Warranty</a>
                                    </div>
                                    <div className="top4-div">
                                        <i className="fas fa-check"></i>
                                        <a className="div-a" href="#">Free Standard Shipping</a>
                                    </div>
                                    <div className="top4-div">
                                        <i className="fas fa-check"></i>
                                        <a className="div-a" href="#">30-Day No Hassle Return</a>
                                    </div>
                                </div>
                                <div className="top-5">
                                    Free Shipping
                                </div>
                            </div>
                            <div className="detail-selection">
                                <Selection />
                            </div>

                        </div>
                    </div>
                </div>
                <div className="description">
                    <Description />
                </div>
                <div className="checkout-bar">
                    <div className="checkout-container">
                        <div className="checkout-chair">
                            <h4>Aeron Chair</h4>
                        </div>
                        <div className="checkout-details">
                            <div className="checkout-logo">
                                <img src={Truck} alt=""/>
                            </div>
                            <div className="checkout-p">In Stock</div>
                            <div className="checkout-price">{`$ ${this.sumFunction()}`}</div>
                            <div className="checkout-input">
                                <input className="quantity-input"
                                       defaultValue="1"
                                       type="text"
                                       onChange={(e)=>this.testFunction(e, this.sumFunction())}  />
                            </div>

                            <div className="checkout-btn">
                                <button>Add To Cart</button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        );
    }
    componentDidMount() {
        this.props.getProductDetails(this.props.productId)

    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps.oneChairCategory !== this.props.oneChairCategory
            && prevProps.subtotal !== this.props.subtotal
            && prevProps.oneChairCategory.price !==this.props.oneChairCategory.price){
            //console.log("chouchou===>", this.props.oneChairCategory)

            this.setState({
                reRender: this.props.oneChairCategory,

            })
        }


    }
}


const mapStateToProps = (state, ownProps) => {
    // console.log("this is final price", state.totalPriceReducer)
    // console.log("productPage--->", ownProps)
    // console.log("check and magic--->", ownProps.history.location.state.chairCategories)
    // console.log('get id--->', ownProps.match.params.id)
    let productId = ownProps.match.params.id
    // console.log("chouchou===>", state.oneChairReducer.oneChairData)

    return {
        productId,
        chairs: state.chairsReducer.allData,
        oneChairCategory: state.oneChairReducer.oneChairData,
        subtotal: state.subTotalReducer.eachSubtotal

    }

}


export default connect(mapStateToProps, {getProductDetails})(ProductPage)

