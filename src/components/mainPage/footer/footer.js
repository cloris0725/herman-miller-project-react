import React, {Component} from "react";
import "../../css/footer/footer.css"



class Footer extends Component{
    render() {
        return(
            <div className="bottom-section">
                <div className="bottom-section-wrapper">
                    <div className="up">
                        <div className="main-info">
                            <div className="bottom-section-left">
                                <div className="column-img">
                                    <img
                                        src="https://store.hermanmiller.com/on/demandware.static/Sites-herman-miller-Site/-/default/dwb444b264/images/logo-footer.svg"
                                        alt="" />
                                </div>

                                <div className="column-wrapper">
                                    <div className="bottom-info-1">
                                        <div className="div-1">Customer Service</div>
                                        <div>Catalog Opt Out</div>
                                        <div>Contact Us</div>
                                        <div>FAQ</div>
                                        <div>Returns and Exchanges</div>
                                        <div>Shipping and Delivery</div>
                                        <div>Warranty and Service</div>
                                        <div>Assembly Instructions</div>
                                        <div>Care and Maintenance</div>
                                        <div>Site FeedBack</div>
                                        <div>Track Your Order</div>
                                        <div>Nelson Product Recall</div>
                                        <div>Our Response to COVID-19</div>
                                    </div>


                                        <div className="bottom-info-2">
                                            <div className="div-1">Resources</div>
                                            <div>For Business</div>
                                        </div>

                                        <div className="bottom-info-1">
                                            <div className="div-1">Locations</div>
                                            <div>Find A Retailer</div>
                                            <div>Our New York Store</div>
                                        </div>



                                    <div className="bottom-info-1">
                                        <div className="div-1">About Herman Miller</div>
                                        <div>About Us</div>
                                        <div>HermanMiller.com</div>
                                        <div>Our Designers</div>
                                        <div>Request A Catalog</div>
                                        <div>Careers</div>
                                        <div>Accessibility</div>
                                        <div>Terms of Sale</div>
                                        <div>Privacy Notice</div>
                                        <div>Cookie Notice</div>
                                        <div>Do Not Sell My Information</div>
                                        <div>Site Map</div>
                                    </div>
                                </div>
                            </div>


                            
                            <div className="bottom-section-right">
                                <div className="bottom-section-right-wrapper">
                                    <div className="wrapper-up">
                                        <div className="div-title">Join our mailing list</div>
                                        <div className="searchBar">
                                            <input type="text"  placeholder="Enter your email"/>
                                            <button>Sign Up</button>
                                        </div>

                                        <div className="div-follow">Follow Us</div>
                                        <div className="div-icons">
                                            <span><i className="fab fa-facebook-f"></i></span>
                                            <span><i className="fab fa-twitter"></i></span>
                                            <span><i className="fab fa-instagram-square"></i></span>
                                            <span><i className="fab fa-pinterest"></i></span>
                                        </div>
                                    </div>
                                    <div className="wrapper-down">
                                        <img src="#" alt=""/>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="country">
                            <div className="country-left">
                                <div className="country-flag">
                                    <img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMSEhUQEhMVFRUVFhUWFRUVFxcVFRUVFxUWGBUVFRUYHiggGBolGxUVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OGhAQGy0fICUtLS0tLS0rKy0tLS0tLS0tLS0tLS0rLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAOEA4QMBEQACEQEDEQH/xAAcAAACAgMBAQAAAAAAAAAAAAADBQIEAAEGBwj/xABIEAABAwEDBA4HBwMEAQUAAAABAAIDEQQSIQUGMVEHEyIyQVJUYXGRkpOx0RQVF1OBodIWI0JywdPhYqLwM0OCo/EkNLLC4//EABsBAQADAQEBAQAAAAAAAAAAAAABAgMEBQYH/8QAOxEAAgEBBAYHBQkAAwEBAAAAAAECAwQRUZESExUhUqEFFBYxU2HRBkGB4fAiMkJxkqKx0uIzYmPxwf/aAAwDAQACEQMRAD8A9xQFTKW8+I8CgFiAa5O3g6T4oA79B6CgEQQF3Jm+PR+qAZIBRbv9R3w8AgBwb5v5h4oB2gKOVNDek+CAXlAOLJvG9CA3at47oKASoBhkv8Xw/VAX0Akn3zvzHxQE7Fv2/HwKAcIBblTfDoQFNAPI9A6AgAZQ3h+HigFSAZ5M3p/N+gQFtAIkAy9YN5+r+UBCWQSi43Tpx0f5igA+r3f0/PyQB45xGLjqkjVzoCRtzThjjh1oCt6vd/T8/JAEhZtW6dw4YYoAvrBuo9Q80ACSzmQl7aUNNOnAUQGMsbmkONKA1PwQB/WDOf5eaAFO7bd7hTTXnQAvQna29f8ACAMy2tYLhrUYGlKIDH21rwWCtXYCqAD6E7WOs+SALC7at9+LRTm6UAX1gzUfl5oAD7G5xLhSjsR0HEcCA3HZjGb7qUGrTqQBvWDefq/lAClbtu6bwYY4IAfq939Pz8kBZ9OaMMcMNGpARknEguNrU69GGKAB6vf/AE9Z8kAaGQRC67SccNWj9EBP09nP1IAHoDtbfmgKaAtZN3//AB/UIBogFOUN+egIADNI6R4oB6gKeU96On9CgFiAbWF33Y+PiUBu0ygNdjwFAcnb8twwiskjGD+pwb4lTFOX3VeQ3d3ihuyHAytxk0vO1lxuH9cpaCOcVXXCwV5b9G78zJ2imveX83c6pbc6RsbGRCMMJL5L53ZeBgwU/AeFUtFlnQu0mt+BNOtGo3deMZMnyuJJtDMdUZ+tcxqZHk+VpBFoZhriP1oC0H2kaDA//k9h6i0j5oCvbbXPgXWeSgriwtkH9hJQFeDKzCaVodRwPUUB09kmBY2h/CPBAbtrtw74eKAUoBlkvenp/RAXUAik0npPiUAfJ+/Hx8EA2QCvKe/H5R4lAVUA8QAPQWaj1lADtDBGLzNNaa8EBX9OfrHUgLMMQkF5wx0YYaEBM2Ngxpox0oCn6c/m6kASzybaS1+gY4YcKAnJZ4xr6ygOQzkz3isjjE0l7hojYA53S41o0dJ+C2pWepV+4ik6kY95wOVs+rXPUX9qafwxYup/VIdPwC9On0fTj/yPSZyztEn9050PcTUA14xq53aOK7YtRV0EkYS397LEdgkficOc+Si9vvK6SR02aOV4cnGV0wleJhEBtbWmhj2wmt544/yXl9KTUYxZ6/Q9iq2yU1TaV13ff6HRjZOsXu7T2Iv3V4vWo4P6+J7vZ61L8Uc36Gjsn2H3dp7EX7qnrMcH9fEjs9asY5v0InZNsPu7T2Iv3VHWoYP6+I7PWrGOb9CPtNsY0MtPdxfuqetQ8/r4kP2ftS98c36ELRsl2CQUkhneP6ooj89tqnWoeZXYNqxjm/QpnZCsjMYDa2f0PjiezoH3tW/NR1qHn9fEbBtOMc36DvN7ZBhtLxC9xY52AEgay/8AldUj4VWsKsZ9xxWno+0Wf/kXxW9fwjtooIzr61ocRqd+10DMAccccUAL05/N1IC4LIw40046SgITQiMX26Rrx0oCt6c/WOpAHs7BILz9INMMMNP6oAvoLNXzKAqemv5kAyvjWEBWyhi3DHEaMdaAW3DqPUUA0sODBXDTp6UAZ7hQ4jQgEhadR6igIHKLIA6SRwa0NxLjdAoca1/wqUr9yB5fnVsiyWhxjsxMcWjbNEsn5B+BvPpPMvSoWFXaVXI5qla7dEo5iZCitcp2yW6GXnPjMZO2BzS0P26/pDnA0I4F0WmvKlC6Cu+JnTgpu+TKtsyM2KV0Eb9tMZLHyOjMQvg4hrC44DDFa05uUVJq743mNS6LLdlsF2hcaD8Tg2+QPxEAkVoFZyu7jK9NnWZUzZjjssc3pFdNXCE1mMmMbQ2/930mq4adpnKo46PM6Z0YqGlfyOHzhjIY38x8FzdKu+CPpfZD/lq/khDdXgaJ95cZdTRFxl1NEES1RokES1RcVcbyBChmbRnMo3p7iJRUlc+46/NXP60WQhkpdNDwgn7xg1tJ3w5iuulafdI+dt/Qqd86PfgesWXLcVqayWFwe0jSPAjgPMu1NPuPmZQlB3SVxaodR6ihUdscKDEaAgA201YQMdHigFZYdR6igGOTjRprhjw4cAQFq+NYQCfancU9X8oAYQFrJu//AOP6hANEApyjvz0BAV2nEdI8UBPOLLsVlidLK4Na0fEngAHCVMYuTuRDdx4NnVnTNb5Kvq2IH7uHSBQ4Ofrd8gvYs9njTV73s5KlRydyHub2ZLJ7JJanWhgIpSrX/clm6lbKBvjQjQoq2mUKijoiNNON94os8bt01rjddhRtWhzQatJGnnXW7nvaOdyu7mPrBYtvkuumDXyYCR7S+8/QAaLGc9CO5XkRWnK5u4c5zZEZZpKNkG6oWx3XEgAUc4uJpStVhQrOonei9Wkoe8XG1SOvXnFwcAHVOkDe4cy30FgZOTfvHGa+RLPa3SNtEd8MaxzRVzaFxcCdyRXABcNvipRSZ6XRVrrWaUnSldf+X/6dB9gMncnHbk+peY6MMEeyumbbx8l6GfYDJ3Jx25PqTU08ENtW7xOS9DPsBk7kw7yT6k1NPBDbVu4+S9CPs+ydyb/sl+pNTTwQ21bePlH0InY+ydyf/tl+pRqYYDbVt4+UfQidj3J3Jv8Asl+pNRTwG2bbx8o+gM7H2TuTf9s31qOr08CNsW3j5R9DR2P8ncn/AO2b606vSw/kbYtnH+2PoahzXishMljaYn8IvyOZIOK8OcQDqcFeMFHuOOtaatd31Hf8Ev4R0ORcriUXTuXNwc06QVcwIPOJ6T4lAWMn78fHwQDZAK8p78flHiUBVQDxAL/V7tY+aAkyLat2414MP56EAQZRbqPyQApYTIb7TQc+nBALcrPEDHSPe0NaLzidAARb9wPC86M45Mo2gPLgyIGkTXYNaD/uSHjH5BexZ6CpR0mt5y1J6TuHVrzKfZrNHanyRVcH3xtgLal1YRCabsuZidStStSlNq5lJ0mleVLBapmBtJHNu3rrQTdbf3+Gh1dBrqXQ6cHva3mOk+5HS5p5v7e8bpoaCb4vUkDaGhaKYitFz2ivq13E06WsZqSzPs8gF9u2M0mN1666lCCaYGtVMZRqxv8Ad5lJLQdyZq+5waxzyWgmhca3bxqTXTSuPWp0Ut6WRGk3ubHVqzadHC2Yvj/EXG/VpB/07mslc8LSpT0bmayoNR0ryebGVIbK6R08gjD2sDa1xILiRo51jbpJRV519GWWrXlJUo33HQfbSw8pZ/nwXma6GJ6+yLb36tm/tpYOUs6/4U62GI2TbPDf18TPtpYeUs6z5JrYYjZNs8N/XxNHPOwcpZ/nwTWwxGybZ4b+viROelg5Sz5+Sa2GI2TbPDf18SJz1sHKW/PyTXQxGybZ4bBvz2yfyhvU5NdDEbJtnhsCc+cn8oHZcmup4jZNr4P4Bvz5yfygdlyjXU8Rsi2cHNCTKWd9hDhNDP8AeN4Lj6PbxXYaU10MQ+irWvwc0dRm7bW2qJs0bwWurrw1gjWtE796OCUHCWjJbx3HBtZvk1A1c+CkqG9Yt1H5IAcke2m83ADDHr4OlAR9XO1j5oA3p7edAXEBUylvPiPAoBWgL9nlux15ygPFtk3L8tsnNhs7XyMi3Ugja57pHjQ0htTdbgfiu+y0opactxjUl+FHP2nN+WzueJo3hrTdD7pDHVANQ44HA/Ir0aVSE1emjlmmtw0dlaedrmyPvRksLGEAMj2sANMQG8wwOtI0YRekkVc3dcNMm5Bne10gifRoa4C47d3j+DWOhUnXhGSTfMhU5NNlyyzyQl4YTG4i64gUkaAa0BO9OtVlGM973lFKUO7cFftkz3Pulz3bpwY0mpoATQc+lFowjc9yDvk7+9lu25JkiJD2mlG7qhu7oYiuvgVY1Yz7mTOm4PeGkt0r2GNzgWEMAZTcsuUu3OKcFCpQTvXeHUk1cc1ng37qP8//ANSuLpH7qPrvY7/lq/kjk6LyD9BuMoguMQGqIRcaoouIuIkKLirREhVaKtA3BUaM2iBVWigNwVTOR0eYGcxsVoDXH7iUgPHEf+GQeB6V1UKt24+b6WsSktZE97faA6KtdXiP4XafNbymEA0yZvT+Y+AQFtAIqoA3pj+N8h5IA1mkMhuvxFK6sfh0oCw6xs4vzKA4bZCy5JZ4zHZwTJIbkLBiS8tJJ6AGuK0px0pJMiTuR4pm/bTDNHaKucWODzRxa5+kkFw4CTiOZezKmnDQOPTad50mcOcj7cYnytuFjXNcGucYyL1Wua06HY0J4VFGiqV6X8FaktIlC0XGPwDXl4YajdFhAeBroSFtem7kYtNd51mb2XXQQbQGlzXF5cS41aHABoh4gC5KtnU56V5pGu4q4p2WEuc2NuL3aBXFxoTXHhoNK1k9FbzBLSe4t5OlDXskqTdcHbk0OGgV4MaV5qqJxbjcTGWi7xplPKTrRdc8ULb2AJLKE4UB/FzrKlRVPzNKlbWFcR0aHfhJIaaihLdIC00lfcjLR94aPNltuBjdI6MR3XAtAJNbwoalcNvi3FK89voPpGVinOUY333EvZPFymTsN815mpfEfS9qKvhrNmeyaHlMnYao1Lx5Idp6vhrNmeyaHlMnYamofFyRHaer4azZr2Txcpk7DVOpfFyQ7T1fDWbM9k8PKZOw1RqHxckO09Xw1myJ2JoeUydhidXfFyRHaar4azZE7E0PKZOwxR1f/tyRHaWr4azZB2xNDymTsMTq3nyRV+0dXgWbBnYmh5TJ2GKOrLHkV7RVeBZg3bE8PKZOw3zTqqxKv2gq8CzZUtOxTFQ/+of2G+alWZL3mc+m5zVzgjpM0pHxEWKaQyXBRriKFzRva04QuhK48aclJ33XHassjD+H5lSVK9qeYzdYaCldeOI4egIAXpr9fyCAv+is1IBZtLuK7qKAs2Fpa6rgQKaTgNIQB7VamhpN4dagHjOV88IW5SlfKBIyBgZGA0OeZnEbY5jjg0Bl6uvALrp2eUo3ozlUSOZypLHJLJIwMEdfuxHGImiMb3cNGnHEnhXr0YaEPtd5x1JXvcags72UcBRwxoQCQeAFpFD0FGr0VvO8ynnJDJZRBG2MSsawXhC1rHF3+uIW/wC2a8PCuKnZpRqaT7vzNp1U43CqysNMK4cNOddbubORnS5sZUbAXOeAWgVa0Mq+/Wm5J0YLltFJzX2f5NqFRQ+9/BmVLQJJXObd2sbwNbdAaQDiBw1qrUoaMd/eUqy0pbu4gxpaa0oQa6OEYjAq7uZRXo6O05XYYbjbokDWmtwXSSRtgZz0quKNCSnpPu/M6pVouFy7yWaDaOl/Kzxcls7kLL3s6dcR2G6ISZRAaQGkBlEBEtQXkHMOpADLDqPUhF6BujOo9SC9FeWEkaD1INJYnN5YidE9k4B3BofynAoL0ddk+1tc0G8OsISDtwLnVaCRQCoxFanUgK+0u4ruooBxeGtATQFPKm8+I8CgOPzpt4hhfIdDGl3UKj50UxV8kiH3Hz/ZiXkuOl5Lj0uNT8z8l7lKNySOSbvOuzbjidaYopmsdGTu77ywMa0Vv1Gk4DDhWtp0tC6PeZwuv3nTZ22mzSSNlszWFsovOkBdeLwbpY6M73ANOjGq5rKqkVozb/Imq4v7onjYCef/AM6V0GB3OQ32X0ZzJGRiSW8A0vfSXaq3S8/7dXVHOvPrKq6l6bZ003DQ33CaM1xApXGgrQc1Tiuz3HK+/cM8mMYZGtka1zXYG84tAGm9eB1LGrfc9HvJpuOkr1ehvlyWGQtkiDSH6XguvVbgG3ToFOFY0FNbpG1dwe+IvYP85l0HOJs77XJFEx0UkkZL6ExvcyounTdOtcFvV8UfXeyNCnVq1VUipbl3pM5T1/a+VWjvpPqXlaPm836n3vULL4UP0r0M9f2vlVo76T6k0fN5v1Gz7L4UP0x9DPX9r5VaO+k+pNHzeb9Rs+y+FD9MfQicvWvlVo76T6lGj5vN+pXZ9l8KH6Y+hE5dtXKrR30n1Jd5vN+pHULL4Uf0r0I+vbVymfvpPqUfF5v1HUbN4cf0r0Iuy3auUz97J9SXebzfqR1GzeHH9K9Abss2nlE/ev8ANRvxebKOx2fw4/pXoV35Sn99L3j/ADVN+LzKuzUeFZIC7KE3vZO27zUNvHmU1FLhWSAvtknvH9t3mq6UsSsqUMCpPM8ggvd2j5qYzd/ecdajFxauPZtj3Ke32WNx31LrvzNJHgAfivRi70fGWinoVGj0PJe8P5j4BWMS4gESAv8ArEcU9YQEJJtt3IFOGulAeW7M0hjsu1g1MsjGfDF5+TafFb2dXzKzdyOZ2Lsl2WaZzrQXt2lt6riwWciSsbA4kXhJedUDhpzLvrzqQS0fmYQjF94LKFijhtEkMRkc2M7WXS3bxLdJAaKAauFddGcpx0pbvyOeokncg9nLQQXVp+K7QOpw0J4VM2/cZ7r952+Vsi2WKyxPa6UvZgW7gPrOb7dvH4KaBzLz6VapKo07rvruOicIKN6YliH8/wCcK7DlOmzUssT5KyFw2vdY02sg7kBxOg1PguW0zlFfZ95tQjGTvfuBW2ysikdEwudc3JL6YmgOAHBRXpyco3syqxUZXLeZHSoJ0cNKVpqHOrXu4okdBabBC2BrwXm6K0F2995Sl8cAC44VZudx2SpwULzmsr5uzW5gjhMYLHBzr5IFCCMKNNcQq25NxSR7Ps30hSsVScqibvu7rvUU+y628ez9t/0LzNCfkfYdp7HwzyXqb9lts49n7b/oTQn5DtPY+GeS/sa9l1t49n7b/oTQn5DtPY+GeS/sa9ltt49n7b/oUaufkR2msnDLJf2NHYstvHs/bk/bUaup5Z/IjtNZOGWS/sa9llt49n7cn7ajV1PLP5EdpbJwyyX9jXsrtvHs/bk/bTV1PLP5EdpbJwyyX9iJ2K7b7yz9uT9tNVU8s/kVftJZOGWS/sQdsVW33lm7cn7ajU1PLMo/aKy8Msl6gnbFVt95Zu3J+2o6vPyKdoLNwyyXqCfsV233ln7b/oUdWn5FX0/ZuGWS9StNsX2wCt+z9476EVmn5GMumrPL3SyXqP8AY5yNNZXSWeUsxcJG3SXDdChGIFN6OtddOLirmeDbK0Ks9KB6fFNtQukVrjXRzfornKE9ZDi/NAa9A50BQQBrEd0eg+IQHm2yjkz0rc32x7S10l594RgAtvl10E7y9waaLehPRkVmtx5bYqA0rebrxAdTQaHrFV7dN+84pI6PIVj257YjK2Nz8GukvOBedDSRiCa4EpOehHSuvKqOk7jpc6shNsktwSNN4NcyIB5eG0Ac5ziLoF4GmPCuez1nVj3E1Kag+8WwuxrU1Ok109Otb7jJnY5DzfE0MkomjNAAKh42p7d08PH5SuKraHCajd/BrCipRvvF7QMWghza4EAgOHAaHgXRf77jne5tIv2SO+4NLw0u3Ic8EiuhoNFSUnFX3XkxWk7m7hvljJohIo9uIaAyhvEjfGugCqwo1XO/ca1qSh7ym08OOOnnpSngtjEfZqjdSflZ4uXHa+5HVZO9nRLjO4xAYgMKAiUBooDEBFwQECgBOQAnoCtMMEBzAN21sOsOHhT9UB1VqNSPyjxKAGUA8QA/RmcUICnlNoYyrRQ6KjVigPBNk3KMrbZGWyOH3bwaHAhxIII0EEVC7LJFPvM6j3FXMvNaS2u3JaI2h94iRl9rg07WDHW8A590V5121a8aa3mMYaRZjjlssgBLWyx03j2yXHaKVbhULpUo1IeRhJOLLjbTJLcje+9QkMMjhubxqQXn8Ncfiq6EYXtIhtvvH9uzXmhhjmdcFQ8yVlZRpvfdhh/GSyhwXPC0xnJx35FpUpJXs1YcoSMDQx5aGkuABoKuGN7jYUCvKnF72jLTktyY3zeyU6dwFW3BUPo4B4FDSjdVSMVlWqqmvMtTpOoze1vheA66Htody4PunRjwVrwKycZrcZyTpyDidz6Nc+ovEguNaF2+x4AVCilvSuDlKW5sZzZIeyNryWjTe3QoOKG8YlYRrpyuNZUGo3tiDLuX5rHG18BaC91115odgASKLG3N6KuPoPZmwUbXUqRq+67uYk9pFv40fdheXpzxPsuztiwefyNe0m38aPux5qdOePIdnbFg8zPaTb+NH3Y800548h2csWDzM9pNv40fdjzUac8eRHZ2xYPM17Sbfxo+7HmmnPHkR2dsWDzNe0q38aPu2+ajWTx5EdnrFg8yJ2S7fx4+7Hmo1lTHl8yr9nrFg8yJ2S7fx4+7HmmsqY8vmR2fseDzIO2S7fx4+7ao1tTHkVfQFjweYJ+yVb+Ozu2qNdUx5FH0FZMHmBdskW/3jOw1Rr5lH0JZMHmBk2Rbef8Acb2GqOsTKS6Hsq9zzMzdzotNotsbZX1G6OAAxpzLopVHJ7zyOkbJSpR+wj3HI7Q9lXbo1pjqww+a6Dxxh6KzihALfSH8YoBn6SzjDrQFHKjw9lG4nTQakB4Nsk5LnktbNrie+kZrdFabpdlklGN95nUV5VyHYsp2a8YIJo3PuBz2xi/dY4uuhx0NJoSOG6NS7JSozS0mY3TXcXGZHtr3ue6ySgvcXOux3W3iauIAOFTj8VrCtSirtJGcoSfuLrc3rXo9Gl1bxHXpcSK6ueA6tMeUZmmOSCQsqwtbteEdwUG16hTArKLoRekpb/zJkqjVzRuzZFtVMbPL2Vd1qfEjN0p4DSwWS1xXjHDI1zgBeDN0AMaCvAVlUlRndpS5lowqQvuQeTJ9oc9zzA8FxvGjaCp0nTw6VCqU1G5SRSVKpJ3tBG5Mn4YX6D+H+UdWHEiupqcIwkZaXNuOidc3NG3MGXdF3FZLUp3pmj1zVzXI5zO/IdpljY2OCRxD6mjdAunnXPbJqUfs7z6b2VrU7LVqOs9FNK685X7H27ksvUPNeZ9rBn2+17F4iM+x9u5LL1DzT7WDG17F4iM+x9u5LL1fyn2sGNr2LxUa+x9u5LL1fyn2sGRtexeKjRzPt/JZeoeai6WDI2vYvFRH7HW/ksvUPNRdLBkbWsXiI0czrfyWXqHmounwv6+JV9LWPxEQOZ1v5JL1fyl08GV2tY/ERB2Z9v5LL1fyo0Z4P6+JV9KWTxF9fAE/NG3cll7P8qNCeDM30nZfEX18AJzTt3JZeyodOeBR9I2bjQN+adt5NL2f5UameBSXSFmf40HzVyRPFbY9sie3B4xbTGi6KMGnvPD6Ur05x+y7z3zIrgyOjiAa1ocMMMV1HhjH0hnGHWgF3ozuKesIACANZN8fynxCA4/Km5tY52H5EfygOjsj8BiUBdYTzpcA7HIAgcgCNcgJVQEgUBsFAYgNoDKoDKoDEBpAZVAaqgNEoCJKC4gSgBvKAE49KEAZnHnQHJWg3rXGNV4//FAdXMNH5R4lCSKAeICl6t/q+QQEJIdqF7fcFNHPp+CA812RcqyQBtpjY0ljhUOvEXXVbwEcJaqydyN7PTjUnoyv+H/xnN2XZUtDRT0eA95+riud2hr3Hsx6IoyX3pZr+pdZssz8mg63+ajrLwNF0JSf4ny9A7NlubksXaep6y8C+wab/G+Qduy3LyWPtv8AJOsPDmWXs9DxHkgjdluTkrO8d5J1iWHMns5HxHkgjdlt/JW9676U18sOZPZqPiPJeoQbLbuSN74/QmvlhzfoT2YXi/t+ZIbLbuSDvj9CnXSw5/IdmF4v7fmSGy2eRjvj+2munhzfoOy//r+3/Rv2tnkY74/tqddLBZ/Inst/6/t/0b9rZ5GO+P7aa+WCz+Q7LLxf2/6Ne1s8jHff/mmulgs/kOyy8X9v+jPa2eRjvj+2mvlgs/kOyy8X9v8Ao0dls8jHfH9tRrp4c36Dsv8A+v7f9Gjstu5IO+P0KNdLDn8h2YXi/t/0ROy27kje+P0KdfLDm/QjswvF/b8yDtlx/JG96foUa+WHMjszHxOXzBu2XJOSs7130p1iWHMjs3DxHkiB2XJOSs7x/knWJYcx2ch4jyBP2W5eSx9t6jrLw5lX7PQX43kgD9lqbksPaenWvIrsGnxvkDm2V5iP/bQ9bz+qjrXkH0FTSv03yLeY2XpLZK+d8bGhpDGhhOmlXVr0tXTCV54dqoxpS0Yts9Ts8O2i9W7Tc006Ma/NXOUN6uHG+SAj6w/pQDBAU8qbz4/oUB5/nxk/bYJGcZpHx0g9YCiSvRelLRmmeFRYYHSMD0hcE1vPrLPUTig7SszujJBmlDeLCNKm9GqkEB51OkjRMmHc6nSWJdMmHKdJYmiZIOU6ccS15u8mnHEk3eTTjiSZeTTjiDV7nTTjiDL3OmnHEGXudNOOJBonnUOccSGyJKaUcSt5Eu51GksSjZAnnUaSKNka86jSRXSBucovM5MHVVZleDtD6AlTBXsxtE9GF56zsZ5N2uzsqMXVeel38UXpwVyPibTPTqNnqeSRuPifAKxgXUAiqgJbc7jO6ygLFicXOo4kilaHEVwQEcrWFrmEXRo1BAeeZPssTJnxOijJBvCrGkkHpCXI0VWou6TXxZ1VnsUFP9CHumeSi5YE6+rxyzfqXI7DD7mLuo/JLkRrqnE82WG2OL3UXds8kuQ1tTiebCNskfu4+wzyS5EayfE82GbAziM7I8kuQ1k8XmyYibxW9kKSuk8SYjbxW9QQXvE3tY1DqCi4Xs3cGodQUi9mXBqHUEF7MuDUOoIL2ZtY1DqCC9mtrGodQUXC9mtrbxW9QU3C9mjG3it7IQaTxBugZxG9keSi4lTkve82BdZI/ds7DfJLkTrJ4vNkDZI/dx9hnklyJ1s+J5sFJY4vdRd2z6UuQ1tTiebK5sUNf9GLumeSXIa2pxPNnMZzxROIibFFVxAwjYOnQNSXIa2pxPM67N/JzWRgXRo1BSZlu2ktcA3cigwGArU44ICvt7uM7rKAcXRqCAV+iP4vzHmgDWWMsN5+ApSqANJaWEUvBAefZ2WZ0cnpLGlzWjdXdJaNNBwlCUr9wvsWyPYQBV0vdnzWbqxXedcbBXlvS5jBmyVk/jy90fNRroGi6LtL9yzCt2TMn8eXuj5pr4Fl0TasFmFGybk/jS90fNRr4eZbY1qwWZIbJuT+NL3R8018fPItsW14LMmNk3J/Gl7v+U18fPInYdrwWZIbJ1g40vdnzUdYj55E7CteCzN+06wcaXuz5p1iPnkTsK14LM2Nk2wcaXuz5p1iPnkNg2vBZme02wa5e7PmnWIeeQ2Da8FmZ7TbBrl7s+adYh55E7BteCzM9ptg40vdnzTrEfPIjYNrwWZr2nWDjS92fNOsR88hsG14LM0dk6wcaXuz5p1iPnkRsK14LMidk3J/Gl7s+adYj55DYdrwWZE7JuT+NL3Z81Ovj55FdiWvBZkTsmZP40vdfymvj5kbFteCzIHZMyfxpe6PmmvgRsa1YLME7ZLyfx5e6PmmvgVfRNqXuWYCXZJsAaTflrwfdHzTXwe4o+jLRFXtLMHm470qZtqo4RY3S4UND+IjnWqd5wyi4u5no9ntEbRS8FJAK1MMhvMxFKVw01OvpQAvQ38X5hAMPSGcYIA6AqZS3nxHgUAsQG7RYw+LrQHz/n3m+bJOXgUjkJpqa7hB6Vz1Ye89no+0/hZzzXLlZ70ZXhGlVNosKChqmTDlJomTDlJopEgVJZMleQtebqpJvN1Qm8yqC81VBeaJUFbzRKEXkaqCrZElQUbIEqCrBkqDJsg5ylGUpDHNvI7rZOIwNw2heebgHSV0Uqe+88bpG1qMdFH0HkzJwigoBTeinRoXYfOb33h2oBpkzen836BAW0AiogLfrE6ggJNl23cHDhw5v/KAn6ubrPyQApZdr3AAI01PP0IDkM7smttEbmOYCDwY9fwTc0WjJxd6OQyVmpYSdrlhcHt0/ePx5xistTE649IWhdz5I6CLMLJx/wBl3ev801EC66UtK96yRZZsfZO9y7vH+aamGBO1bVxLJB27H2TvcnvH+aamGA2tauLkEbsf5O9we29NTDAbWtXFyCDMDJ/uP73JqYYDa1r4iQzByf7j+9yamGBO17XxEhmFk/3H9zk1MMBta18Zv7BZP9wO05NTDAbWtfHyM+wWT/cDtOTUwwG1rXx8jX2Cyf7j+5yamGA2ta+PkR+wOT/cf3uTUwwG17XxGHMHJ/uP73JqYYDa9r4iBzAyf7j+9yamGA2va+LkDdsf5O9we27zTUwwG17XxckAkzAyd7l3eP8ANNTDAbWtfFyQB+YGTvcu72RNTDAjatq4lkhHljNTJ8Yo2FxccGjbH4nnxTUwKvpO0vvkskOcy8jNs7aNYBiSdJPSTwrRJRRxzqSm75bzuoZb/wB2RQHhHNjwqSgf1cNZ+SAg+TatwMa449X6IDXrE6h80AT0BusoBcgLWTd//wAf1CAaIBTlHfnoCApujrhzhAKM5cgEnbI8HDEEfqgFFjzhjiNy0SMicOO4MDucE6VDaRaMJS7leNYs5bHyuz99H5ppRxL6irwPJllmc1j5XZu+j81GnHEnq9bglkwzc5bHyyzd/H5ppxxQ6tX4JZMmM5bHyyzd9H5prIYonqtfgl+lkvtNY+WWbvo/NRrIYonqlfw5ZM2M5bFyyzd9H5prIYodUtHhyyZv7S2Lllm76PzTWQxQ6paPDlkzPtNYuWWbvo/NNZDFDqlo8OWTNHOWxcss3fR+aayGKHVLR4csmROc1i5ZZu/j801kcUOq1+CWTM+0tj5ZZu+j801kMUOqV/Dl+lkDnNYuWWbv4/NTpxxRXqtbglkwbs5LJyuzd/H5ppxxRPVa/BLJlabOax8Frs/fR+anTjiV6vV4Hkxdbc5oHbiGWOV50Nje156TdOARNPuKypyj95NFjN/IjpH7dLiT1AagpKD5kIbUDWgLmT9+Pj4IBugFmU9+PyjxKAqFAPEBlwah1ICtlDBuGGI0YIBdth1nrPmgGdhFWAnHTp6UAV8YocBo1IBFKC4UJKA5nLeazLSHNc2opw8B1g8BUNJlozlF3o8hzizVmsjjQF8fA4DEczh+oXPOlgezZbenunuEbXLnaPXhUTCNcq3GqmTDlFxqpkg5LjRTJX0J00ZeQnTRl5QNNGXkI00QLlJm5GVQm/FkS5TcUc0gb3qUjCdUZZCyBNa3AMF1vC8jD4DhK3hSbPKtNvjDdHez1nN7M5lmDaDEipP4nHWV1RSR4lSpKo75M7GBl0UBKkzH0UYoMBoHBzIAdtFGEjDRo6UAsMh1nrPmgGGThVprjjw48AQFq4NQ6kAo252s9aAYenM1/IoAdokEgusxNa6sPj0oCt6E/V8wgLUEojF1xodOvSgJm2MOAOnmKApegv1fMICUUNypfgCKa0AuyrYYZQa8P9JQHmWX9j1rnmVjXAHgYQ291tNOpUdNM6aVrq09yeZRsuaVirdmdao3c74qde1Kupgb7Tr+WXzHEOx1YXYiW0kc0kX7SjUQLbVtHlk/UO3Y0sXvLT3kX7SdXgSul7R/1yfqEGxpYveWntxftKOrw8y22LT/ANcn6m/ZnYuPae3F+0nV4eZO2bRhHJ+pnszsPvLV3kX7SdXh5kbZtOEcn6mvZrYveWnvIv2k6tDzG2bThHJ+pB2xpYveWnvIv2lPV4eZD6XtD4cn6gZdjmwgVMlpHTJF+0moiV2raPLJ+onteaNgG5jfapHamviI69qTUxK7Tr+WT9QmSNj1peJHtfcH4Xua7rutCsqcUZVLbVqK5vI9OyPkuGIADg/pWhyDeWG/QxioGB4EBr0F+r5hAXW2tgwJ0YaCgITzCRpY01J0DRoxQFU2J+r5hAWLNIIxdfga114aODoQBfTWa/kUBU9DcgKqAtZN3/8AxPiEA0QCrKG/PQEBXZpHSEA9QFLKm9HT+hQC2iAYWazh0YqNfiUAqyrm/G9rsOAoDi5c3nx4xPczoOHUgJWeS2tqNzIBrBB6wUBYblmZu/gPS136EIDPtQ0GjopR/wAQf1QGHOhhwbHMf+IHiUBjstyneQO/5OA+QCABaLTbHUADGV5iT8ygBsyDLLjLI93NWg6ggOwyRm3GxrdyNAQDmayNbG6g4EBQAQDLJe9PT+iAuoBHKcT0nxQBrBvx8fBANkAsynvx+UeJQFMoB6gK3q9ut3y8kBCWIRC83E6MdHy6EAL1g/U3qPmgCxwiUX3VB0YaMOlASNgaManDHg8kBX9YP1N6j5oAkT9tN12AGOHVwoAvq9ut3y8kACS0GMljaUGvTjj+qA022OcQ0gUOB06D8UAU5MYdfy8kBVtNkbHi0VrpvY6OiiApyRg/hb1fygJNzejeLxGJxwpTwQGOzdiYC4DEYitKeCAhHEB+FvV/KAu2axtk3wpd0XcNPSgLLcls5/l5IAZtbmm6AKNJA06AeHFAbjtJkNwgUOmmnRVAH9Xt1u+XkgBSu2rBuNcccfCiAh6wfqb1HzQBxYWnGpxx4OH4ICMkAjF9tajXoxw4EAL1g7U3qPmgCxRiUXnYEYYaKaeHpQE/V7dbvl5ICv6c7U3qPmgGaAqZS3nxH6oBYgGuT94Pj4oA79B6CgEQQF3Jm+PR+qAZIBRbt+74eAQA7Pvm9I8UA7QFHKmhvxQC5AObINw3oQGWkbh3QUAmQDDJf4vh4IC+gEk++d+Y+KAnYt+34+BQDhALcqb4dCApoB5HoHQEADKG8Pw8UApqgGmTN6fzHwCAtoBHT/KFAaQFvJ++PR+qAYIBbb9/8PNABh3w6QgHCAq5Q3vxQC4oBrY94OgeAQBX6CgEyAu5N4UBdQCq0793SgMsu/b0nwQDVAUspfhQFEoByzQEBC1bw9B8EApQDDJu9/zWgLaAUT74/H9EAWw75AMkAuyjvh0DxKArj9R4oBwgP//Z" alt=""/>
                                </div>
                                <div className="country-div"><a href="#">United States</a></div>
                            </div>
                            <div className="country-centre">
                                <span>© 2020 Herman Miller, Inc. </span>
                            </div>
                        </div>
                    </div>

                    <div className="down">
                        <div> A HermanMiller Group Company</div>
                    </div>

                </div>
            </div>

        )
    }
}

export default Footer