import React, {Component} from "react";
import "../../css/header/header.css"
import User from "../../../img/User.svg"
import ShoppingCart from "../../../img/ShoppingCart.svg"
import MegaMenuContent from "./megaMenuContent";
import MegaMenu from "./megaMenu";


class Header extends Component{

    constructor(props) {
        super(props);
        this.state={
            showOverlayNew:false
        }
    }

    overlayMenu = () => {
        const {showOverlayNew} = this.state
        this.setState({
            showOverlayNew: !showOverlayNew
        })
    }



    render() {

        const displayOrNot = display => {

            return {visibility: display ? 'visible' : 'hidden'}
            //
        }

        return(

            <div className="header">
                <div className="top-section">

                    <div className="promo-bar">
                        <div className="promo-bar-centre">Free Shipping on Office Chairs &nbsp; + &nbsp; 0% Financing
                            Available
                        </div>
                    </div>


                    <div className="functions">
                        <div className="functions-center">
                            <div className="function-1">
                                <li className="li-left"><a href="">Store</a></li>
                                <li className="li-right"><a href="">Contract</a></li>

                            </div>
                            <div className="function-2">
                                <div>
                                    <a href="">Customer Service &nbsp;&nbsp; 888 798 0202</a>
                                </div>

                            </div>

                            <div className="function-3">

                                <div className="login">
                                    <div className="login-top">
                                        <li>My Account</li>
                                        <img src={User} alt=""/>
                                    </div>
                                    <div className="login-dropdown">
                                        <div className="div-login">Login</div>
                                        <div>Register</div>
                                    </div>
                                </div>

                                <div className="login2">
                                    <li>Cart</li>
                                    <img src={ShoppingCart} alt=""/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="search-menu-bar">
                    <MegaMenu />
                </div>

            </div>





        )
    }
}

export default Header