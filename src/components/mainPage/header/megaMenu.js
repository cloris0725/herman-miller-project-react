import React, {Component} from "react";
import "../../css/header/header.css"
import MegaMenuContent from "./megaMenuContent";


class MegaMenu extends Component{
    render() {
        return(
            <div className="search-menu">
                <div className="search-menu-wrapper">
                    <div className="logo"><a href="#"><img
                        src="https://store.hermanmiller.com/on/demandware.static/Sites-herman-miller-Site/-/default/dw82523a93/images/logo.svg"
                        alt="" /></a></div>
                    <div className="menu-list">
                        <a className="menu"
                            // onMouseOver={this.overlayMenu} onMouseOut={this.overlayMenu}
                           href="#">
                            <li>New</li>
                            <div className="dropdownMenu">
                                <MegaMenuContent />
                            </div>

                        </a>
                        <a href="" className="red-line">
                            <li>Office</li>
                        </a>
                        <a href="">
                            <li>Living</li>
                        </a>
                        <a href="">
                            <li>Dining</li>
                        </a>
                        <a href="">
                            <li>Bedroom</li>
                        </a>
                        <a href="">
                            <li>Outdoor</li>
                        </a>
                        <a href="">
                            <li>Lighting</li>
                        </a>
                        <a href="">
                            <li>Accessories</li>
                        </a>
                        <a href="">
                            <li>Gaming</li>
                        </a>

                    </div>
                    <div className="input">
                        <input id="input" type="text" placeholder="Search" />
                    </div>
                </div>
            </div>

        )
    }
}


export default MegaMenu
