import React, {Component} from "react";

class MegaMenuContent extends Component{

    chairPage = props => {
        let test = this.props.chairs
        console.log('check--->', test)
        // console.log('props--->', props.match.params)
        // const {clickedImgId} = props.match.params
        // console.log('new history--->', props.match.params)
        // let history = useHistory()
    }

    render() {
        return(
            <div className="menu-main-content">
                <div className="overlay-left">
                    <div>See All New Arrivals</div>
                    <div>View All</div>
                </div>
                <div className="overlay-right">
                    <div className="overlay-r-img">
                        <img className="r-img" src="https://store.hermanmiller.com/on/demandware.static/-/Sites-herman-miller-Library/default/dwf26d7127/megamenu-images/2019-holiday-megamenu/11112019-new.jpg" alt=""/>
                    </div>

                    <div className="overlay-r-content">
                        <div className="p-title">Eames Fiberglas Side Chair</div>
                        <div>Original Eames colors are out of the archives
                            and better than ever in fiberglass</div>
                        <div className="flex-div">
                            <div>Shop Now </div>
                            <div><i className="fas fa-long-arrow-alt-right"></i></div>
                        </div>
                    </div>
                </div>

            </div>

        )
    }

}

export default MegaMenuContent


