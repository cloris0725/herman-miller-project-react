import React, {Component} from "react";
import "../../css/main/main.css"
import getAllPictures from "../../../action/action";
import {connect} from "react-redux"
import Products from "./mainProductsDisplay";
import {BrowserRouter, Route, Link, useHistory, useLocation} from "react-router-dom";




class MainPageSortLine extends Component{

    constructor(props) {
        super(props);
        this.state={
            showListP: false,
            showListM: false,
            showListF: false
        }
    }

    componentDidMount() {
        this.props.getAllPictures()
    }

    componentDidUpdate(prevProps, prevState, snapshot) {

    }


    showListP = (show) => {
        console.log(show)
        const {showListP} = this.state

        this.setState({
            showListP: !showListP,
            showListM: false,
            showListF: false
        })
    }


    showListM = (show) => {
        console.log(show)
        const {showListM} = this.state
        this.setState({
            showListP: false,
            showListM: !showListM,
            showListF: false
        })
    }


    showListF = (show) => {
        console.log(show)
        const {showListF} = this.state

        this.setState({
            showListP: false,
            showListM: false,
            showListF: !showListF
        })
    }



    render() {

        const displayOrNot = show => {

            return {display: show ? 'flex' : 'none'}
        //
        }

        return(
            <div className="main-content">
                <div className="main-content-header">
                    <div className="page-menu"><a href="">Home > Office > Office Chair</a></div>
                    <div><h2>Office Chair</h2></div>
                    <div className="sort-line" >
                        <div className="sort-line-left">
                            <div className="dropdown-menu-1" >
                                <div className="dmenu-1" onClick={this.showListP}>
                                    <div className="dmenu-1-title">price:</div>
                                    <div className="triangle-down"/>
                                </div>
                                <ul style={displayOrNot(this.state.showListP)}>
                                    <div className="price-selection">
                                        <div className="square-price-list">
                                            <div>
                                                <i className="far fa-square" />
                                                <li>$500 or less</li>
                                            </div>
                                            <div>
                                                <i className="far fa-square" />
                                                <li>$500 - $1000</li>
                                            </div>
                                            <div>
                                                <i className="far fa-square" />
                                                <li>$1000 - $2000</li>
                                            </div>
                                            <div>
                                                <i className="far fa-square" />
                                                <li>$2000 - $3000</li>
                                            </div>
                                            <div>
                                                <i className="far fa-square" />
                                                <li>$3000 - $5000</li>
                                            </div>
                                            <div>
                                                <i className="far fa-square" />
                                                <li>Above $5000</li>
                                            </div>
                                        </div>

                                        <div className="close-btn" onClick={this.showListP}>close</div>
                                    </div>
                                </ul>

                            </div>
                            <div className="dropdown-menu-2">
                                <div className="dmenu-2" onClick={this.showListM}>
                                    <div className="dmenu-1-title"> material:</div>
                                    <div className="triangle-down" />
                                </div>
                                <ul style={displayOrNot(this.state.showListM)}>
                                    <div className="material-selection">
                                        <div className="material-list">
                                            <div>
                                                <i className="far fa-square" />
                                                <li>Fabric</li>
                                            </div>
                                            <div>
                                                <i className="far fa-square" />
                                                <li>Leather</li>
                                            </div>
                                            <div>
                                                <i className="far fa-square" />
                                                <li>Plastic</li>
                                            </div>
                                            <div>
                                                <i className="far fa-square" />
                                                <li>Combination</li>
                                            </div>
                                            <div>
                                                <i className="far fa-square" />
                                                <li>Epic</li>
                                            </div>
                                            <div>
                                                <i className="far fa-square" />
                                                <li>MCL Leather</li>
                                            </div>
                                        </div>

                                        <div className="close-btn" onClick={this.showListM}>close</div>
                                    </div>
                                </ul>
                            </div>
                            <div className="dropdown-menu-3">
                                <div className="dmenu-3" onClick={this.showListF}>
                                    <div>sort by:</div>
                                    <div className="flex">
                                        <div className="flex-top">
                                            <div>Featured Products</div>
                                            <div className="triangle-down"/>
                                        </div>
                                        <div className="flex-bottom">
                                            <div className="select-list" style={displayOrNot(this.state.showListF)}>
                                                <div className="select">
                                                    <div>Price: High to Low</div>
                                                    <div>Price: Low to High</div>
                                                    <div>Name: A to Z</div>
                                                    <div>Name: Z to A</div>
                                                    <div>Average Rating</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                        <div className="sort-line-right">
                            <div className="showing-item" >
                               showing 30 of 30 items
                            </div>
                            <div className="showing-icon">
                                <i className="fas fa-th-large" />
                                <i className="fas fa-th" />
                            </div>
                        </div>
                    </div>
                </div>
                <div className="products">
                    <div className="all-chair" >
                        <Route path="/" component={Products} />
                    </div>
                </div>

            </div>

        )
    }
}

const mapStateToProps = (state) => {
    // console.log(state.chairsReducer.allData)
    return {
        chairs: state.chairsReducer.allData

    }

}


export default connect(mapStateToProps, {getAllPictures})(MainPageSortLine)