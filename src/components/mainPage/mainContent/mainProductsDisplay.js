import React, {Component} from "react";
import {connect} from "react-redux";

import {BrowserRouter, Route, Link, useHistory, useLocation} from "react-router-dom"



class MainProductsDisplay extends Component{


    jumpToProductPage = (index) => {

        let id = this.props.chairs[index].id

        this.props.history.push(
            `/productPage/${id}`
        )


    }


    render() {

            return (this.props.chairs?.map((chair, index) => {
                    const img = chair.media[0]
                    // console.log(img)
                    const colorsArray = chair.profileCategories[0].profileItems
                    // console.log(chair.profileCategories[0])
                    // console.log("colorsArray--->", colorsArray, index)
                    // console.log('colorsArray -->', colorsArray)
                    // console.log('!!!!==>', chair)

                    return (
                        <div className="single-chair" key={index} onClick={event => this.jumpToProductPage(index)} >
                            <div className="chair-top">
                                <img src={img}  alt=""  />
                            </div>
                            <div className="chair-details">
                                <div className="d-name" >{chair.name}</div>
                                <div className="d-price">{`$${chair.price}`}</div>
                                <div className="d-color">
                                    {colorsArray.map((color, index) => {
                                        // console.log('colorsArray -->', colorsArray, index)
                                        return(
                                            <div className="small-color" key={ index }>
                                                <img className="s-color-img" src={colorsArray[index].media} alt=""/>
                                            </div>
                                        )
                                    })}
                                </div>
                                <div className="d-free">Free Shipping</div>
                            </div>
                        </div>

                    )
                })

            )
        }



    //}

}


const mapStateToProps = (state, oweProps) => {

    //ownProps.match.params.id ---->能拿到路由后面push的index


    // console.log('checkcheck',state.chairsReducer.allData)
    return {
        chairs: state.chairsReducer.allData,
    }

}

export default connect(mapStateToProps, null)(MainProductsDisplay)


