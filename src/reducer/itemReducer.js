import React from "react";
import {ONE_Item} from "../helper";


const initState ={
    eachProfileItem: []
}

const eachItemReducer = (state = initState, action) =>{
    // console.log("hi", action.payload)

    switch (action.type){
        case ONE_Item:
            return{
                ...state,
                eachProfileItem: action.payload
            }
        default:
            return state

    }

}


export default eachItemReducer
