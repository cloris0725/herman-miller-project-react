
import {combineReducers} from "redux";
import {GET_ALL_IMG} from "../helper";
import {GET_ONE_CHAIR_ARR} from "../helper";
import profileReducer from "./profileReducer"
import eachItemReducer from "./itemReducer";
import subTotalReducer from "./subTotalReducer";
import totalPriceReducer from "./finalPriceReducer";




const initState ={
    allData: [],
    oneChairData: []
}



const chairsReducer = (state= initState, action) => {
    switch (action.type){
        case GET_ALL_IMG:
            return {
                ...state,
                allData: action.payload}

        default:
            return state

    }

}


const oneChairReducer = (state= initState, action)=>{
    // console.log("payload====>", action.payload)
    switch (action.type){

        case GET_ONE_CHAIR_ARR:
            return{
                ...state,
                oneChairData:action.payload
            }


        default:
            return state
    }
}


export default combineReducers({
    chairsReducer,
    oneChairReducer,
    profileReducer,
    eachItemReducer,
    subTotalReducer,
    totalPriceReducer

    }


)