import {GET_TOTAL} from "../helper";

const initState = {
    finalPrice: []
}

const totalPriceReducer = (state=initState, action) => {
    console.log("payload===>", action.payload)

    switch (action.payload){
        case GET_TOTAL:
            return{
                ...state,
                finalPrice: action.payload
            }

        default:
            return state
    }

}


export default totalPriceReducer

