// import {combineReducers} from "redux";
import {ONE_CATEGORY} from "../helper";


const initState = {
    // eachProfileItem: {},
    eachCategory: []
}


const oneCategoryReducer = (state=initState, action) => {
    // console.log("payload===>", action.payload)
    switch (action.type){
        case ONE_CATEGORY:
            return{
                ...state,
                eachCategory: action.payload
            }
        default:
            return state

    }
}




export default oneCategoryReducer