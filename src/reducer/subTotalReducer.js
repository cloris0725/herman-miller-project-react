import {GET_SUBTOTAL} from "../helper";

const initState ={
     eachSubtotal: []
}


const subTotalReducer = (state=initState, action) => {

    switch (action.type){
        case GET_SUBTOTAL:
            // console.log("payload===>", action.payload)
            return{
                ...state,
                eachSubtotal: action.payload
            }

        default:
            return state
    }
}




export default subTotalReducer

